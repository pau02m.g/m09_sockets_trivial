package server;

public class PauException extends Exception {
	
	public PauException(String err) {
		super(err);
	}

}
