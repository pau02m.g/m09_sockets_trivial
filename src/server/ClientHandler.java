package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.sun.source.tree.SynchronizedTree;

public class ClientHandler implements Runnable {

	Socket socket;
	PrintWriter out;                   
    BufferedReader in;
    ServerManager m_sm;
    
    Manager m_manager;
    
    int respuestaJugador;
    int puntos = 0;
    
    String nick;
    
    public ClientHandler(Socket socket, ServerManager sm) throws IOException
    {
    	this.socket = socket;
    	out = new PrintWriter(socket.getOutputStream(), true);                   
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        m_sm = sm;
        m_manager = new Manager(this.socket);
    }
    
    
    int estado = 0;
    
    
	public void run()
	{
		System.out.println(Thread.currentThread().getName() + ": Connection established with "+ socket.getInetAddress() +" at port " + socket.getPort() + " and local port " + socket.getLocalPort());
	
		try {
			m_manager.sendAndReceive(Protocolo.BYTE_WELCOME, Protocolo.BYTE_ACK);
			
			nick = "";
			boolean flag = true;
			while(flag) {
				nick = m_manager.receive();
				if(m_sm.comprobarnombre(nick)) {
					m_manager.sendByte(Protocolo.BYTE_ACK);
					flag = false;
				}else {
					m_manager.sendByte(Protocolo.BYTE_NICK_UNAVAILABLE);
				}
			}
			
			
			
			boolean partida = true;
			boolean lobby = true;
			while(partida) {
				lobby = true;
				estado = 0;
				while(lobby) {
					if(m_sm.pucJugar()) {

						System.out.println("TENDRIA QUE EMPEZAR UNA PARTIDA EN 5 --> ClientHandler");
						synchronized (m_sm.connectar) {
							System.out.println(">>> >>> notifay de conectar");
							m_sm.connectar.notifyAll();
						}
						lobby = false;
					}
					else {
						System.out.println("Esperando y tal");
						m_manager.sendAndReceive(Protocolo.BYTE_ON_GAME, Protocolo.BYTE_ACK);
						synchronized(m_sm.lobby) {
							m_sm.lobby.wait();
						}	
					}
				}
				
				m_manager.sendAndReceive(Protocolo.BYTE_QUEUE, Protocolo.BYTE_ACK);
				//esperar que terminie lacuentaatras
				synchronized(m_sm.comenzar) {
					m_sm.comenzar.wait();
				}
				// envio el json
				m_manager.sendAndReceive(m_sm.getPreguntaActual(), Protocolo.BYTE_ACK);
				
				// me llega la respuesta del cliente
				estado = 1;
				respuestaJugador = m_manager.receiveInt();
				m_manager.sendByte(Protocolo.BYTE_ACK);
				// compruevo la respuesta
				if(respuestaJugador == m_sm.getCorrectaActual()) {
					m_manager.sendByte(Protocolo.BYTE_ANSWER_CORRECT);
					puntos++;
				}
				else
					m_manager.sendByte(Protocolo.BYTE_ANSWER_WRONG);
				
				// pongo la puntuacion
				m_sm.PonerPuntuacion(nick, puntos);
				
				m_manager.receiveByte(Protocolo.BYTE_ACK);
				
				// notificamos de que el cliente ya a respondido
				synchronized(m_sm.jugat) {
					m_sm.jugat.notify();
				}
				
				// me espero y tal
				synchronized (m_sm.allPlayers) {
					m_sm.allPlayers.wait();
				}

				String resultado = m_sm.DarPuntuacion();
				// envio el resultado y tal
				m_manager.sendAndReceive(resultado, Protocolo.BYTE_ACK);
				
				//pregunto si quiere continuar
				m_manager.sendAndReceive(Protocolo.BYTE_CONTINUE, Protocolo.BYTE_ACK);
				
				switch(m_manager.receiveByte()) {
					case Protocolo.BYTE_CONTINUE_YES:
						System.out.println("El cliente " + nick + " se QUEDA");
						break;
					case Protocolo.BYTE_CONTINUE_NO:
						System.out.println("El cliente " + nick + " se va (Y se va t.t)");
						partida = false;
						break;
				}
				m_manager.sendByte(Protocolo.BYTE_ACK);
				System.out.println("el estado de la partida es: " + partida);
			}
			
			//m_manager.close();
			
			
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.err.println("El " + nick + " es un payaso");
			
			if(estado == 1) {
				
				try {
					m_manager.sendInt(5);
					respuestaJugador = m_manager.receiveInt();
					
					m_manager.receiveByte(Protocolo.BYTE_ACK);
					m_manager.sendByte(Protocolo.BYTE_ANSWER_WRONG);
					m_manager.receiveByte(Protocolo.BYTE_ANSWER_WRONG);
					// pongo la puntuacion
					m_sm.PonerPuntuacion(nick, puntos);
					m_manager.sendByte(Protocolo.BYTE_ACK);
					m_manager.receiveByte(Protocolo.BYTE_ACK);
					
					
					// notificamos de que el cliente ya a respondido
					synchronized(m_sm.jugat) {
						m_sm.jugat.notify();
					}
					
					// me espero y tal
					synchronized (m_sm.allPlayers) {
						m_sm.allPlayers.wait();
					}
	
					
					String resultado = m_sm.DarPuntuacion();
					// envio el resultado y tal
					m_manager.send(resultado);
					m_manager.sendByte(Protocolo.BYTE_ACK);
					m_manager.receiveByte(Protocolo.BYTE_ACK);
					
					
					//pregunto si quiere continuar
					m_manager.sendByte(Protocolo.BYTE_CONTINUE);
					m_manager.receiveByte(Protocolo.BYTE_CONTINUE);
					
					m_manager.sendByte(Protocolo.BYTE_ACK);
					m_manager.receiveByte(Protocolo.BYTE_ACK);
					
					m_manager.sendByte(Protocolo.BYTE_CONTINUE_NO);
					m_manager.receiveByte(Protocolo.BYTE_CONTINUE_NO);
						
					//partida = false;
							
					
					m_manager.sendByte(Protocolo.BYTE_ACK);
					m_manager.receiveByte(Protocolo.BYTE_ACK);
				}
				catch (PauException e1) {
					e1.printStackTrace();
				} 
				catch (InterruptedException e1) {
					e1.printStackTrace();
					
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				

				
			}
		} 
		catch (PauException e) {
			e.printStackTrace();
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
			
		}
		
		
		
		
	}

}
