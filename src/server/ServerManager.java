package server;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.source.tree.SynchronizedTree;

public class ServerManager implements Runnable{


	private HashSet<String> nombres = new HashSet<String>();
	
	private ArrayList<String> preguntas = new ArrayList<String>();
	private ArrayList<Integer> respuesta = new ArrayList<Integer>();
	
	Object lobby = new Object();
	Object connectar = new Object();
	Object comenzar = new Object();
	Object jugat = new Object();
	Object allPlayers = new Object();
	
	Object pucJugarSync = new Object();
	
	private boolean partidaEnCurso = false;
	private int nJugadores = 0;
	
	Random r = new Random();
	private String preguntaActual;
	private int correctaActual;
	
	StandartWrapper sw = new StandartWrapper();
	
	
	
	@Override
	public void run() {
		preguntas();
		try {
			
		
			while(true) {
				partidaEnCurso = false;
				
				//avisar alos que staban en espera
				System.out.println(">>> VA HE EMPEZAR UNA PARTIDA");
				synchronized(lobby) {
					lobby.notifyAll();
				}
				
				//me espero al primer jugador
				System.out.println(">>> ESPERANDO A QUE ALGUIEN ENTRE A PARTIDA");
				synchronized(connectar)
				{
					System.out.println(">>> lento");
					connectar.wait();
					System.out.println(">>> ya va");
				}
				
				sleep();
				//preparar json de preugnmats 
				
				SeleccionarPregunta();
				synchronized(comenzar)
				{
					comenzar.notifyAll();
				}
				partidaEnCurso = true;
				
				// espero a que todos los jugadores me respondan la pregunta (ojo)(si un jugador se desconecta cuando tendria que mirar como hacer para que no pete)
				EsperaraJugaadores();
				
				// cuando todos han contestado digo cosas
				synchronized (allPlayers) {
					System.out.println(">>> Se notifica a todos los jugadores");
					allPlayers.notifyAll();
				}

				
			}
		}catch(InterruptedException e)
		{
			System.out.println(e);
		}
	}
	
	
	
	public synchronized boolean comprobarnombre(String n) {
		for(String s : nombres)
			if(s.equals(n))
				return false;
				
		
		nombres.add(n);
		return true;
	}
	
	
	public boolean pucJugar() {
		synchronized(pucJugarSync)
		{
			if(partidaEnCurso == false) {
				nJugadores++;
				//connectar.notifyAll();
				return true;
			}
			
			return false;
		}
	}

	
	private void sleep() {
		System.out.println("LA PARTIDE EMPIEZA EN...");
		try {
			System.out.println("5");
			Thread.sleep(1000);
			System.out.println("4");
			Thread.sleep(1000);
			System.out.println("3");
			Thread.sleep(1000);
			System.out.println("2");
			Thread.sleep(1000);
			System.out.println("1");
			Thread.sleep(1000);
			System.out.println("YA");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void preguntas() {
		preguntas.add("{\"pregunta\":\"cuanta shermanas tiene el ruben\",\"respostes\":[\"1\",\"2\",\"3\",\"no tiene hermana\"]}");
		preguntas.add("{\"pregunta\":\"es c=c\",\"respostes\":[\"si\",\"no\",\"if(c<c)-->syso(hey)\",\"yo me llamo xavi\"]}");
		preguntas.add("{\"pregunta\":\"quien es Tuty\",\"respostes\":[\"gordo\",\"barras\",\"pucho\",\"ksio\"]}");
		preguntas.add("{\"pregunta\":\"quien es casca\",\"respostes\":[\"ns\",\"el Hector\",\"la de berserk\",\"lo que canta Roju\"]}");
		preguntas.add("{\"pregunta\":\"bro yout jordan\",\"respostes\":[\"are fake as fuck\",\"son falses de collons\",\"are looking good\",\"MID andy\"]}");
		
		respuesta.add(1);
		respuesta.add(4);
		respuesta.add(2);
		respuesta.add(2);
		respuesta.add(4);
		
		
	}
	
	private void SeleccionarPregunta() {
		int num = r.nextInt(preguntas.size());
		
		preguntaActual = preguntas.get(num);
		correctaActual = respuesta.get(num);
		
	}
	
	public String getPreguntaActual() {
		return preguntaActual;
	}
	
	public int getCorrectaActual() {
		return correctaActual;
	}
	
	
	private void EsperaraJugaadores() { // mirar porqueco�o la gente es subnormal: alt f4
		while(nJugadores > 0) {
			synchronized(jugat) {
				try {
					jugat.wait();
					nJugadores--;
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println(" >>> Ya han respondido todos");
	}
	
	
	public synchronized void PonerPuntuacion(String nick, int puntuacion) { //fijate la locura que voy a cometer: utilizar lo que estaba haciendo ya de normal
		Resultado res = new Resultado();
		res.setNick(nick);
		res.setPuntuacion(puntuacion);
		sw.getResultats().add(res);
		
	}
	
	public synchronized String DarPuntuacion() throws JsonProcessingException { //fijate la locura que voy a cometer: utilizar lo que estaba haciendo ya de normal (2)
		ObjectMapper m = new ObjectMapper();
		return m.writeValueAsString(sw);
	}
	
	
	
	
}
