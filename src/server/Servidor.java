package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.fasterxml.jackson.databind.ObjectMapper;


public class Servidor {

	public static void main(String[] args){


    	
        int portNumber = 60099;
        
        try
        (
            ServerSocket serverSocket = new ServerSocket(portNumber);
        ) {
        	ExecutorService executors = Executors.newCachedThreadPool();
        	//ejecuta el manager, no tiene socketsni nada, s�lo gestiona la partida
        	
        	ServerManager sm = new ServerManager();
        	executors.execute(sm);
        	while(true)
        	{
        		System.out.println("Esperant que es connecti un client");
        		Socket clientSocket = serverSocket.accept();
        		executors.execute(new ClientHandler(clientSocket, sm));
        	}
        } catch (IOException e)
        {
            System.out.println("Exception caught when trying to listen to port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }

	}

}
