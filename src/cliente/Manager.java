package cliente;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Manager {

	PrintWriter out;
	BufferedReader in;
	DataOutputStream outStream;
	DataInputStream inStream;

	public Manager(Socket echoSocket) throws IOException {
		out = new PrintWriter(echoSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
		outStream = new DataOutputStream(echoSocket.getOutputStream());
		inStream = new DataInputStream(echoSocket.getInputStream());

	}

	void send(String message) {

		out.println(message);

	}

	void send(byte message) throws IOException {

		outStream.writeByte(message);

	}

	void send(int message) throws IOException {

		outStream.writeInt(message);

	}
	
	

	String receive() throws IOException {

		return in.readLine();

	}
	
	byte receiveByte() throws IOException {

		return inStream.readByte();

	}
	
	int receiveInt() throws IOException {

		return inStream.readInt();

	}
	
	

	void receive(String message) throws IOException, PauException {

		String hey = receive();
		if (!hey.equals(message)) {
			throw new PauException("Esperaba " + message + " y recibi " + hey);
		}

	}

	void receiveByte(byte message) throws IOException, PauException {

		byte hey = receiveByte();
		if (hey != message) {
			throw new PauException("Esperaba " + message + " y recibi " + hey);
		}
	}
	
	void receiveByte(int message) throws IOException, PauException {

		int hey = receiveInt();
		if (hey != message) {
			throw new PauException("Esperaba " + message + " y recibi " + hey);
		}
	}
	
	
	// STRING
	
	String sendAndReceive(String message) throws IOException {

		send(message);
		return receive();

	}
	
	byte sendAndReceiveByte(String message) throws IOException {

		send(message);
		return receiveByte();

	}
	
	int sendAndReceiveInt(String message) throws IOException {

		send(message);
		return receiveInt();

	}
	
	// BYTE
	
	String sendAndReceive(byte message) throws IOException {

		send(message);
		return receive();

	}
	
	byte sendAndReceiveByte(byte message) throws IOException {

		send(message);
		return receiveByte();

	}
	
	int sendAndReceiveInt(byte message) throws IOException {

		send(message);
		return receiveInt();

	}
	
	
	// INT
	String sendAndReceive(int message) throws IOException {

		send(message);
		return receive();

	}
	
	byte sendAndReceiveByte(int message) throws IOException {

		send(message);
		return receiveByte();

	}
	
	int sendAndReceiveInt(int message) throws IOException {

		send(message);
		return receiveInt();

	}
	
	

	void sendAndReceive(String message, String response) throws IOException, PauException {

		send(message);
		receive(response);

	}
	
	void sendAndReceive(String message, byte response) throws IOException, PauException {

		send(message);
		receiveByte(response);

	}

	void sendAndReceiveByte(byte message, byte response) throws IOException, PauException {

		send(message);
		receiveByte(response);

	}
	
	void sendAndReceive(int message, byte response) throws IOException, PauException {

		send(message);
		receiveByte(response);

	}

	void receiveAndSend(String message, String response) throws IOException, PauException {

		receive(message);
		send(response);

	}

	void receiveAndSend(byte message, byte response) throws IOException, PauException {

		receiveByte(message);
		send(response);

	}
	
	void receiveAndSend(int message, byte response) throws IOException, PauException {

		receiveByte(message);
		send(response);

	}

	void open(Socket socket) {

	}

	void close() throws IOException {
		in.close();
		out.close();
		inStream.close();
		outStream.close();
	}

}
