package cliente;

import java.util.ArrayList;

public class StandartWrapper {

	String pregunta;

	ArrayList<String> respostes;

	ArrayList<Resultado> resultats;

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public ArrayList<String> getRespostes() {
		return respostes;
	}

	public void setRespostes(ArrayList<String> respostes) {
		this.respostes = respostes;
	}

	public ArrayList<Resultado> getResultats() {
		return resultats;
	}

	public StandartWrapper() {
		super();
	}

	public StandartWrapper(String pregunta, ArrayList<String> respostes, ArrayList<Resultado> resultats) {
		super();
		this.pregunta = pregunta;
		this.respostes = respostes;
		this.resultats = resultats;
	}

	public void setResultats(ArrayList<Resultado> resultats) {
		this.resultats = resultats;
	}

	@Override
	public String toString() {
		return "StandartWrapper [pregunta=" + pregunta + ", respostes=" + respostes + ", resultats=" + resultats + "]";
	}

}
