package cliente;

import java.io.*;
import java.net.*;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Cliente {

	public static void main(String[] args) throws PauException, ParseException {

		String hostName = "10.1.82.10";
		// String hostName = "127.0.0.1";
		int puerto = 60009;

		try (Socket echoSocket = new Socket(hostName, puerto);
				BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {

			Manager m_manager = new Manager(echoSocket);
			m_manager.receiveAndSend(Protocolo.BYTE_WELCOME, Protocolo.BYTE_ACK);
			String nombre = "Ruben viene el Kane?";
			m_manager.send(nombre);

			// BENVINGUT
			boolean flag = true;

			// NICK
			boolean nick = true;

			while (nick) {

				switch (m_manager.receiveByte()) {
				case Protocolo.BYTE_NICK_UNAVAILABLE:
					System.err.println("El nick '" + nombre + "' esta en uso");
					nombre = stdIn.readLine();
					m_manager.send(nombre);
					break;
				case Protocolo.BYTE_ACK:
					nick = false;


					break;
				}
			}

			while (flag) {
				boolean enCurso = true;
				while(enCurso) {
					switch(m_manager.receiveByte()) {
					case Protocolo.BYTE_ON_GAME:
						m_manager.send(Protocolo.BYTE_ACK);
						break;
					case Protocolo.BYTE_QUEUE:
						enCurso = false;
						
						m_manager.send(Protocolo.BYTE_ACK);
						break;
					}
					
				}
				

				String json = m_manager.receive();
				
				MostrarJson(json);
				m_manager.send(Protocolo.BYTE_ACK);

				String numero = stdIn.readLine();
				int num = Integer.parseInt(numero);
				
				m_manager.sendAndReceive(num, Protocolo.BYTE_ACK);
				
				switch (m_manager.receiveByte()) {
				case Protocolo.BYTE_ANSWER_CORRECT:
					System.out.println(" _______________CORRECTO__________________");
					m_manager.send(Protocolo.BYTE_ACK);
					break;
				case Protocolo.BYTE_ANSWER_WRONG:
					System.out.println(" ________________INCORRECTO_________________");
					m_manager.send(Protocolo.BYTE_ACK);
					break;
				}
				String json_r = m_manager.receive();
				System.out.println(json_r);
				
				
				LeerPuntuacion(json_r);
				
				
				
				m_manager.send(Protocolo.BYTE_ACK);

				m_manager.receiveAndSend(Protocolo.BYTE_CONTINUE, Protocolo.BYTE_ACK);
				QuieresSeguir();

				String decision = stdIn.readLine();
				switch (decision) {
				case "1":
				case "a":
				case "SI":
					System.out.println("A DICHO SI");
					m_manager.send(Protocolo.BYTE_CONTINUE_YES);
					m_manager.receiveByte(Protocolo.BYTE_ACK);
					break;
				default:
					System.out.println("A DICHO NO");
					m_manager.send(Protocolo.BYTE_CONTINUE_NO);
					m_manager.receiveByte(Protocolo.BYTE_ACK);
					flag = false;
					System.out.println("FINS DESPRES");
					break;
				}

			}
			// CERRAR LA CONEXIOn

		} catch (UnknownHostException e) {
			System.err.println("Don't know about host " + hostName);
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void MostrarJson(String json) {

		ObjectMapper mapper = new ObjectMapper();
		StandartWrapper Wrapper = null;

		try {
			Wrapper = mapper.readValue(json, StandartWrapper.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(Wrapper.getPregunta());
		System.out.println();
		int cont = 1;
		for (String string : Wrapper.getRespostes()) {
			System.out.println(cont + " - " + string);
			cont++;
		}
	}


	private static void QuieresSeguir() {
		System.out.println(" _________________________________");
		System.out.println("|                                 |");
		System.out.println("|         QUIERES SEGUIR          |");
		System.out.println("|_________________________________|");
		System.out.println("|                |                |");
		System.out.println("|     a) SI      |      b) NO     |");
		System.out.println("|________________|________________|");
	}

	
	public static void LeerPuntuacion(String json) throws FileNotFoundException, IOException, ParseException {
		System.out.println("Entro a LeerPuntuacion");
		JSONParser parser = new JSONParser();
		// pillo el json
		JSONObject ListaJson = (JSONObject) parser.parse(json);

		System.out.println(ListaJson.get("resultats") + "");
		
		JSONArray jugadores = (JSONArray) ListaJson.get("resultats");
		
		for(Object p : jugadores) {
			JSONObject m_puntuacion = (JSONObject) p;
			System.out.println(m_puntuacion.get("nick") + " --> " + m_puntuacion.get("puntuacio"));
		}
		
		
		System.out.println("Salgo a LeerPuntuacion");
	}
}
