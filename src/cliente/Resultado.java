package cliente;

public class Resultado {
	String nick;
	int puntuacio;
	
	

	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public int getPuntuacion() {
		return puntuacio;
	}
	public void setPuntuacion(int puntuacion) {
		this.puntuacio = puntuacion;
	}

	
	public Resultado() {
		super();
	}
	
	public Resultado(String nick, int puntuacion) {
		super();
		this.nick = nick;
		this.puntuacio = puntuacion;
	}
	
	@Override
	public String toString() {
		return "Resultado [nick=" + nick + ", puntuacio=" + puntuacio + "]";
	}
	
	
	

}
